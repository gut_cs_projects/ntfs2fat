#pragma once

class MFT
{
	HeaderNTFS fsHeader;
	BootSectorNTFS bootSector;
	FILE* file;
	long mftBegin;

	std::vector<std::pair<RecordHeaderNTFS, std::vector<NTFSAttrCustom>>> records;

	static  constexpr const char fileType[]= { 'F','I','L', 'E' };

	// return <offsetByteNumber, clusterByteNumber>, 
	std::pair<Byte, Byte> readDataRunSize(const Byte& byte)
	{
		Byte offsetSize = (byte & 0x10) >> 4;
		Byte clusterByteNumber = byte & 0x01;

		return std::make_pair(offsetSize, clusterByteNumber);
	}

	Byte* readDataRun()
	{
		Byte sizeByte;
		fread(&sizeByte, sizeof(Byte), 1, file);

		if (sizeByte == 0)
		{
			return nullptr; // no more data run
		}

		Qword dataRunOffsets=0;
		Qword dataRunSize = 0;
		Word dataBlockCluster = 0, clusterOffset = 0;
		auto splitedSize = readDataRunSize(sizeByte);

		fread(&clusterOffset, sizeof(Byte), splitedSize.first, file);
		fread(&dataBlockCluster, sizeof(Byte), splitedSize.second, file);

		dataRunOffsets = clusterOffset* bootSector.bytesPerSector * bootSector.sectorPerCluster;
		dataRunSize = splitedSize.first + splitedSize.second;
		Byte* dataRun = new Byte[dataRunSize];
		long ptr = ftell(file);

		fseek(file, dataRunOffsets, SEEK_SET);
		fread(dataRun, sizeof(Byte), dataRunSize, file);
		fseek(file, ptr, SEEK_SET);
		return dataRun;
	}
	
	NTFSAttrCustom readAttribute(FILE* file, RecordHeaderNTFS header)
	{
		Dword type, length;
		NTFSAttrCustom attr;
		long ptr = ftell(file);

		fread(&type, sizeof(Dword), 1, file);
		
		if (type == END_ATTR_LIST) // sprawdamy czy koniec argumentow
		{
			attr.type = END_ATTR_LIST;
			attr.length = -1;
			return attr;
		}

		fseek(file, ptr, SEEK_SET);
		fread(&attr, sizeof(NTFSAttrCustom), 1, file);
	
		if (attr.formCode == 0) // Resident
		{
			fseek(file, ptr, SEEK_SET);

			NTFSAttributeResident attrResident;
			fread(&attrResident, sizeof(NTFSAttributeResident)-sizeof(Byte*), 1, file);

			int contentSize = attrResident.length - sizeof(NTFSAttributeResident)+sizeof(Byte*);
			attrResident.data = new Byte[attrResident.contentLength];
			fseek(file, attrResident.contentOffset, SEEK_CUR);

			//if(attrResident.type == FILE_NAME)
			//	fseek(file, attrResident.nameOffset, SEEK_CUR);

			fread(attrResident.data, attrResident.contentLength, 1, file);

			fseek(file, ptr + attrResident.length, SEEK_SET);
			return attrResident;
		}
		else
		{
			fseek(file, ptr, SEEK_SET);

			NTFSAttributeNonResident attrNonResident;
			fread(&attrNonResident, sizeof(NTFSAttributeNonResident) - sizeof(Byte*), 1, file);
			Byte* byte = nullptr;
			fseek(file, ptr + sizeof(NTFSAttributeNonResident) - sizeof(Byte*) + attrNonResident.runlistOffset, SEEK_SET);
			do
			{
			byte = readDataRun();

			}
			while (byte != nullptr);

			attrNonResident.data = byte;
			//attrNonResident.data = new Byte[contentSize];
			//fread(attrNonResident.data, contentSize, 1, file);
			fseek(file, ptr+attrNonResident.length, SEEK_SET);

			return attrNonResident;
		}
		////fread(&length, sizeof(type), 1, file);
		//Byte* data = new Byte[length - 8];

		//fread(data, 1, length - 8, file);
		//atrr.fill(type, length, data);
		//return atrr;
	}

public:
	MFT(const HeaderNTFS& fsHeader, const BootSectorNTFS &bootSector, FILE* file, long mftBegin) :
		file(file),
		bootSector(bootSector),
		fsHeader(fsHeader),
		mftBegin(mftBegin)
	{}

	void addRecord(RecordHeaderNTFS& recordHeader, std::vector<NTFSAttrCustom>& attrs)
	{
		records.push_back(std::make_pair(recordHeader, attrs));
	}

	void readMFT()
	{
		RecordHeaderNTFS recordHead;

		fseek(file, mftBegin, SEEK_SET);
		
		long beginPTR = ftell(file);

		//for (int i = 0 i < 16; i++)
		//{
		//	beginPTR = ftell(file);
		//	fread(&recordHead, sizeof(RecordHeaderNTFS), 1, file); // odczytaj header
		//	fseek(file, beginPTR, SEEK_SET); // wroc do poczatku rekordu
		//	fseek(file, recordHead.bytesAllocated, SEEK_CUR); // przeskocz do nastepnego rekord
		//	beginPTR = ftell(file); // zapisz pozycje w pliku
		//}
		
		fseek(file, 1024* 20, SEEK_CUR); // odczytaj header
		beginPTR = ftell(file);

		fread(&recordHead, sizeof(RecordHeaderNTFS), 1, file); // odczytaj header

		int i = 4;
		do
		{
			fseek(file, recordHead.attributesOffset - sizeof(RecordHeaderNTFS) , SEEK_CUR);
			addRecord(recordHead, getRecordAttrs(recordHead)); // znajdz atrybuty

			fseek(file, beginPTR, SEEK_SET); // wroc do poczatku rekordu
			fseek(file, recordHead.bytesAllocated, SEEK_CUR); // przeskocz do nastepnego rekord
			beginPTR = ftell(file); // zapisz pozycje w pliku
			fread(&recordHead, sizeof(RecordHeaderNTFS), 1, file); // odczytaj header
			i++;
		} while (std::equal(recordHead.recordType,recordHead.recordType+4, MFT::fileType));

	//	fseek(file, beginPTR + sizeof(RecordHeaderNTFS), SEEK_SET); // 
	}

	std::vector<NTFSAttrCustom> getRecordAttrs(const RecordHeaderNTFS& header)
	{
		std::vector<NTFSAttrCustom> attrs;
		int attributesSize = 0;

		while (attributesSize < header.bytesAllocated)
		{
			NTFSAttrCustom attr = readAttribute(file, header);
			if (attr.length == -1)
				break;

			attrs.push_back(attr);
			attributesSize += attr.length; // dodajemy rozmiar 
		}
		return attrs;
	}
};

class NTFS
{
	HeaderNTFS fsHeader;
	BootSectorNTFS bootSector;
	MFT* mft;
	FILE* file;

public:
	NTFS(const 	HeaderNTFS& fsHeader, const  BootSectorNTFS& bootSector) :
		fsHeader(fsHeader),
		bootSector(bootSector)
	{
		mft = new MFT(fsHeader, bootSector, file, calculateMFTCluster());
	}

	NTFS(const std::string& path)
	{
		file = fopen(path.c_str(), "rb");
		fread(&bootSector, sizeof(BootSectorNTFS), 1, file);
		fread(&fsHeader, sizeof(HeaderNTFS), 1, file);
		mft = new MFT(fsHeader, bootSector, file,calculateMFTCluster());
	}

	~NTFS()
	{
		delete mft;
	}

	NTFS()
	{}

	void readRecords()
	{
		mft->readMFT();
		mft->readMFT();
	}
	long calculateMFTCluster()
	{
		return fsHeader.mftCluster*bootSector.sectorPerCluster * bootSector.bytesPerSector;
	}
};