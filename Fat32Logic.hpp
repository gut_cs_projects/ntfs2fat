#pragma once

using namespace FAT;

#define END_OF_FAT  0x0FFFFFF8
#define BIT_MASK28	0x0FFFFFFF
#define BIT_MASK4	0xF0000000
#define FREE_FAT	0x00000000

class FATTable
{
	FatEBP fsHeader;
	FatBPB bootSector;
	FILE* file;
	long tableBegin;
	// 4byte per entry
	std::vector<std::vector<int>> records;

	std::pair<Byte, Dword> normalizeTo28Bit(const Dword& dword)
	{
		return std::make_pair(dword & BIT_MASK4, dword & BIT_MASK28);
	}

	Dword getCurentCluster(const long& offset = 0) const
	{
		return (ftell(file) - offset) / (bootSector.bytesPerSector * bootSector.sectorsPerCluster);
	}

public:
	// add  cluster addres to the FAT arra and return index block
	int addFatEntry(const int& index, const Dword& clusterNumber)
	{
		Dword entry, valueNormalised=0;
		long ptr = ftell(file);
		int blockIndex = 2;
		
		fseek(file, tableBegin, SEEK_SET);
		fread(&entry, sizeof(Dword), 1, file); // reserved
		fread(&entry, sizeof(Dword), 1, file); //reserved
		fread(&entry, sizeof(Dword), 1, file);

		auto entryNormalised = normalizeTo28Bit(entry);

		do
		{
			if (entryNormalised.second == FREE_FAT) // entry is free
			{
				valueNormalised = entryNormalised.first | normalizeTo28Bit(clusterNumber).second;

				fseek(file, ftell(file) - sizeof(Dword), SEEK_SET);
				fwrite(&valueNormalised, sizeof(Dword), 1, file);
				fseek(file, ptr, SEEK_SET);
				return blockIndex;
			}
			fread(&entry, sizeof(Dword), 1, file);
			blockIndex++;
			entryNormalised = normalizeTo28Bit(entry);
		} while (entryNormalised.second < END_OF_FAT);

		fseek(file, ptr, SEEK_SET);
		return  -1;
	}

public:
	FATTable(const FatEBP& fsHeader, const FatBPB &bootSector, FILE* file, long tableBegin) :
		file(file),
		bootSector(bootSector),
		fsHeader(fsHeader),
		tableBegin(tableBegin)
	{}
};

class RootDirectory
{
	FatBPB bootSector;
	FatEBP fsHeader;
	FILE* file;
	long directoryBegin;

public:
	RootDirectory(const FatBPB& bootSector, const FatEBP& fsHeader, FILE* file, const long& dirBegin):
	file(file),
	bootSector(bootSector),
	fsHeader(fsHeader),
	directoryBegin(dirBegin)
	{}
	
	Dword addDirsRecord(const LongFileName& lfn, const FatDirectory& directory)
	{
		long currentPtr = ftell(file);
		FatDirectory dir;
		fread(&dir, sizeof(FatDirectory), 1, file);

		do
		{
			if (dir.attributes != LFN && dir.shortName == FREE_FAT)
			{
				fread(&dir, sizeof(FatDirectory), 1, file);

				if (dir.attributes != LFN && dir.shortName == FREE_FAT)
				{
					fseek(file, ftell(file) - 2* sizeof(FatDirectory), SEEK_SET);
					fwrite(&lfn, sizeof(lfn), 1, file);
					fwrite(&directory, sizeof(FatDirectory), 1, file);

					fseek(file, currentPtr, SEEK_SET);
					return getCurentCluster(2 * sizeof(FatDirectory));
				}			
			}
			fread(&dir, sizeof(FatDirectory), 1, file);
		} while (true);

		fseek(file, currentPtr, SEEK_SET);
		return getCurentCluster();
	}

	Dword getCurentCluster(const long& offset=0) const
	{ 
		return (ftell(file) - offset) / (bootSector.bytesPerSector * bootSector.sectorsPerCluster);
	}

	// Add fatDirectory and return adres byte
	Dword addDirRecord(const FatDirectory& directory)
	{
		long currentPtr = ftell(file);
		FatDirectory dir;
		fread(&dir, sizeof(FatDirectory), 1, file);

		do
		{
			if (dir.attributes != LFN && dir.shortName == FREE_FAT)
			{
				fseek(file, ftell(file) - sizeof(FatDirectory), SEEK_SET);
				fwrite(&directory, sizeof(FatDirectory), 1, file);
				fseek(file, currentPtr, SEEK_SET);

				return getCurentCluster(sizeof(FatDirectory));
			}

			fread(&dir, sizeof(FatDirectory), 1, file);
		} while (true);


		fseek(file, currentPtr, SEEK_SET);
		return getCurentCluster();
	}

	FatDirectory translate(const NTFSRecord& record)
	{
		FatDirectory result;
		return result;
	}
};

class DataSector
{
	FatBPB bootSector;
	FatEBP fsHeader;
	FILE* file;
	long dataBegin;

public:
	DataSector(const FatBPB& bootSector, const FatEBP& fsHeader, FILE* file, long dataBegin) :
		file(file),
		bootSector(bootSector),
		fsHeader(fsHeader),
		dataBegin(dataBegin)
	{}

	void addCluster(const long& idx, Byte * cluster, int count=1)
	{
		long ptr = ftell(file);
		long clusterSize = bootSector.bytesPerSector*bootSector.sectorsPerCluster;
		
		fseek(file, dataBegin + idx * clusterSize, SEEK_SET);
		
		fwrite(&cluster, clusterSize, count, file);
		
		fseek(file,ptr, SEEK_SET);
	}
};

class FAT32
{
	FatBPB bootSector;
	FatEBP fsHeader;
	FATTable* table;
	RootDirectory *rootDir;
	DataSector *dataSector;
	FILE* file;

	static const int fatVersion = 32;
public:
	FAT32(const FatEBP& fsHeader, const  FatBPB& bootSector) :
		fsHeader(fsHeader),
		bootSector(bootSector)
	{
	}

	FAT32(const std::string& path)
	{
		file = fopen(path.c_str(), "rb+");
		fread(&bootSector, sizeof(FatBPB), 1, file);
		fread(&fsHeader, sizeof(FatEBP), 1, file);

		table = new FATTable(fsHeader, bootSector, file, bootSector.reservedSectors*bootSector.bytesPerSector);
		rootDir = new RootDirectory(bootSector, fsHeader, file, calculateRootDirectory());
		dataSector = new DataSector(bootSector, fsHeader, file, calculateFirstDataSector());
	}

	~FAT32()
	{
		delete table;
	}

	
	NTFSAttrCustom * getAttr(std::vector<NTFSAttrCustom >& attrs, NTFS_ATTR_TYPE type)
	{
		for (auto attr : attrs)
		{
			if (attr.type == type)
			{
				return &attr;
			}
		}
	}

	Dword getAttrSize(std::vector<NTFSAttrCustom > &attrs)
	{
		Dword sum = 0;

		for (auto attr : attrs)
		{
			sum += attr.length;
		}

		return sum;
	}
	void save(std::pair < RecordHeaderNTFS, std::vector<NTFSAttrCustom>>& record)
	{
		FatDirectory dir;
		Dword filesize;

		if(record.first.flags == NTFS_FILE_FLAGS::Hidden)
			dir.attributes = FAT_FILE_FLAGS::Hidden;

		dir.fileSize = getAttrSize(record.second);
		
	}

	FAT32()
	{}

	long calculateFirstDataSector() const
	{
		auto rootDirSector = ((bootSector.rootDirEntries*FAT32::fatVersion) + bootSector.bytesPerSector - 1) / bootSector.bytesPerSector;
		return bootSector.reservedSectors + (bootSector.fatCopies* bootSector.sectorsPerFat * bootSector.bytesPerSector) + rootDirSector;
	}
	
	long calculateRootDirectory() const
	{
		return fsHeader.rootDirectoryClusterNumber* bootSector.bytesPerSector* bootSector.sectorsPerCluster;
	}

	long calculateFatSector() const
	{
		return bootSector.reservedSectors*bootSector.bytesPerSector;
	}
};