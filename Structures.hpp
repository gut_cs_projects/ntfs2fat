//#pragma once
//#pragma pack(push, 1)
//
//enum NTFS_FILE_FLAGS
//{
//	ReadOnly	= 0x0001,
//	Hidden		= 0x0002,
//	System		= 0x0004,
//	Archive		= 0x0020,
//	Device		= 0x0040,
//	Normal		= 0x0080,
//	Tmp			= 0x0100,
//	Sparse		= 0x0200,
//	Reparse		= 0x0400,
//	Compressed	= 0x0800,
//	Offline		= 0x1000,
//	NotIndexed	= 0x2000,
//	Encrypted	= 0x4000
//};
//
//enum NTFS_ATTR_TYPE
//{
//	STANDARD_INFO			= 0x10,
//	ATTR_LIST				= 0x20,
//	FILE_NAME				= 0x30,
//	OBJECT_ID				= 0x40,
//	SECURITY_DESCRIPTOR		= 0x50,
//	VOLUME_NAME				= 0x60,
//	VOLUME_INFORMATION		= 0x70,
//	DATA					= 0x80,
//	INDEX_ROOT				= 0x90,
//	INDEX_ALLOCATION		= 0xA0,
//	BITMAP					= 0xB0,
//	REPARSE_POINT			= 0xC0,
//	EA_INFORMATATION		= 0xD0,
//	EA						= 0xE0,
//	LOGGED_UTILITY_STREAM	= 0x100,
//	END_ATTR_LIST			= 0xffffffff
//};
//
//
//template<const int Size>
//struct Sector
//{
//    Byte data[Size];
////}__attribute__ ((__packed__));
//};
//template<const int Size>
//struct Cluster
//{
//    Byte data[Size];
////}__attribute__ ((__packed__));
//};
//
//struct BootSectorNTFS
//{
//	Byte jmp[3];
//	char oem[8];
//	Word bytesPerSector;
//	Byte sectorPerCluster;
//	Word reservedSectorCount;
//	Byte tableCount;
//	Word rootEntryCount;
//	Word sectorCount;
//	Byte mediaType;
//	Word sectorPerTable;
//	Word sectorPerTrack;
//	Word heads;
//	Dword hiddenSectorCount;
//	Dword sectorCount32;
//	Dword reserved;
//	Qword sectorCount64; // 64 bity
////}__attribute__ ((__packed__));
//};
//
//struct HeaderNTFS
//{
//	Qword mftCluster;
//	Qword mftMirrorCluster;
//	Byte clusterPerRecord; // klastry na rekord, gdy x<0 rozmiar rekordu = 2<<x
//	Byte reserved1[3];
//	Byte clusterPerIndexBuffer;
//	Byte reserved2[3];
//	Qword serialNumber;
//	Dword checksum;
//	//}__attribute__ ((__packed__));
//};
//
//struct RecordHeaderNTFS
//{
//	char recordType[4];
//	Word updateSequenceOffset; 
//	Word updateSequenceLength; 
//	Qword logFileSequenceNumber; 
//	Word recordSequenceNumber;
//	Word hardLinkCount;
//	Word attributesOffset;
//	Word flags;
//	Dword bytesInUse;
//	Dword bytesAllocated;
//	Qword parentRecordNumber;
//	Dword nextRecordNumber;
//	Dword reserved;
//	Qword recordNumber;
//	//}__attribute__ ((__packed__));
//};
//
//struct NTFSAttribute
//{
//	Dword type;
//	Dword length;
//	Byte formCode; // resident 0, non resident 1
//	Byte nameLength;
//	Word nameOffset;
//	Word flags;
//	Word attrID;
//	Byte* data;
//
//
//	void fill(Dword type, Dword length, Byte* data)
//	{
//		this->data = data;
//		this->length = length;
//		this->type = type;
//	}
//
//	void destroy()
//	{
//		delete[] data;
//	}
//};
//
//struct NTFSAttrCustom
//{
//	NTFS_ATTR_TYPE type;
//	Dword length;
//	Byte formCode; // resident 0, non resident 1
//	Byte nameLength;
//	Word nameOffset;
//	Word flags;
//	Word attrID;
//};
//
//struct NTFSAttributeResident:public NTFSAttrCustom
//{
//	Dword contentLength;
//	Word contentOffset;
//	Word unUsed;
//	Byte* data;
//};
//
//struct NTFSAttributeNonResident: public NTFSAttrCustom
//{
//	Qword startVirtualCluster;
//	Qword endingVirtualCluster;
//	Word runlistOffset;
//	Word compressionUnitSize;
//	Dword reserved;
//	Qword attrContentSize;
//	Qword sizeOnDiskAttrContent;
//	Qword initializedSizeOfAttrContent;
//	Byte* data; //data runs  if first byte 00 END of runs
//};
//
//
//class NTFSRecord
//{
//	RecordHeaderNTFS header;
//	std::vector<NTFSAttribute> attrs;
//
//public:
//	NTFSRecord(const RecordHeaderNTFS& header, const std::vector<NTFSAttribute>& attrs) :
//		header(header),
//		attrs(attrs)
//	{}
//	NTFSRecord()
//	{}
//};
//
//
//struct FatBPB
//{
//	Byte JMP[3];
//	Byte oem[8];
//	Word bytesPerSector;
//	Byte sectorsPerCluster;
//	Word reservedSectors;
//	Byte fatCopies;
//	Word rootDirEntries;
//	Word numSectors;
//	Byte mediaType; // ?
//	Word sectorsPerFat; // Fat16Only, not used in fat32
//	Word sectorsPerTrack;
//	Word numberOfHeads;
//	Dword hiddenSectors;
//	Dword sectorBig; //This field is set if there are more than 65535 sectors in the volume, resulting in a value which does not fit in the Number of Sectors entry at 0x16.
////}__attribute__ ((__packed__));
//};
//
//struct FatEBP
//{
//	Dword sectorPerFat;
//	Word flags;
//	Word fatVersion;
//	Dword rootDirectoryClusterNumber;
//	Word fsInfoSector;
//	Word backupBootSectorNumber;
//	Byte Reserved[12]; // set to 0
//	Byte driveNumber;
//	Byte flagsWindows;
//	Byte signature;
//	Dword volumeID;
//	Byte volumeLabel[11];
//	Qword systemString; // = "FAT32"
//	Byte bootCode[420];
//	Word bootPartitionSignature;
//	//}__attribute__ ((__packed__));
//};
//
//enum FAT_FILE_FLAGS
//{
//	ReadOnly = 0x01,
//	Hidden = 0x02,
//	System = 0x04,
//	VolumeID = 0x08,
//	Directory = 0x10,
//	Archive = 0x20,
//	LFN = 0x0f,
//	Deleted = 0xe5,
//	E5 = 0x05,
//	Subdirectory = 0x2e
//};
//
//struct FatRecor
//{
//	Byte shortName[11];
//	FAT_FILE_FLAGS attributes;
//	/*  01 tylko do odczytu
//	02 plik ukryty 04 plik systemowy '
//	08 etykieta dysku
//	10 katalog
//	20 znacznik archwizacji
//	0f dluga nazwa
//	e5 plik skasowany
//	05 pierwszym znakiem jest e5h
//	2e opis podkatalogu; je�li nast�pny bajt zawiera tak�e kod 2EH (tj.
//'..'), to analizowana pozycja opisuje katalog nadrz�dny w
//stosunku do bie��cego � w tym przypadku pole 'Nr pocz�tkowej
//pozycji ...' wskazuje na numer pozycji w FAT, zawieraj�cej
//katalog nadrz�dny; zero oznacza, �e katalogiem nadrz�dnym
//jest katalog g��wny
//	*/
//	Byte reserved;
//	Byte time10ms;
//	Word fileCreationTime;
//	Word fileCreationDate;
//	Word lastAccessTime;
//	Word fat32Start;
//	Word lastModifiedTime;
//	Word lastModifiedDate;
//	Word fat32End;
//	Dword fileSize;
//	//}__attribute__ ((__packed__));
//};
//#pragma pack(pop)