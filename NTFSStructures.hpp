#pragma once
#pragma pack(push, 1)

enum NTFS_FILE_FLAGS
{
	ReadOnly = 0x0001,
	Hidden = 0x0002,
	System = 0x0004,
	Archive = 0x0020,
	Device = 0x0040,
	Normal = 0x0080,
	Tmp = 0x0100,
	Sparse = 0x0200,
	Reparse = 0x0400,
	Compressed = 0x0800,
	Offline = 0x1000,
	NotIndexed = 0x2000,
	Encrypted = 0x4000
};

enum NTFS_ATTR_TYPE
{
	STANDARD_INFO = 0x10,
	ATTR_LIST = 0x20,
	FILE_NAME = 0x30,
	OBJECT_ID = 0x40,
	SECURITY_DESCRIPTOR = 0x50,
	VOLUME_NAME = 0x60,
	VOLUME_INFORMATION = 0x70,
	DATA = 0x80,
	INDEX_ROOT = 0x90,
	INDEX_ALLOCATION = 0xA0,
	BITMAP = 0xB0,
	REPARSE_POINT = 0xC0,
	EA_INFORMATATION = 0xD0,
	EA = 0xE0,
	LOGGED_UTILITY_STREAM = 0x100,
	END_ATTR_LIST = 0xffffffff
};

struct BootSectorNTFS
{
	Byte jmp[3];
	char oem[8];
	Word bytesPerSector;
	Byte sectorPerCluster;
	Word reservedSectorCount;
	Byte tableCount;
	Word rootEntryCount;
	Word sectorCount;
	Byte mediaType;
	Word sectorPerTable;
	Word sectorPerTrack;
	Word heads;
	Dword hiddenSectorCount;
	Dword sectorCount32;
	Dword reserved;
	Qword sectorCount64; // 64 bity
//}__attribute__ ((__packed__));
};

struct HeaderNTFS
{
	Qword mftCluster;
	Qword mftMirrorCluster;
	Byte clusterPerRecord; // klastry na rekord, gdy x<0 rozmiar rekordu = 2<<x
	Byte reserved1[3];
	Byte clusterPerIndexBuffer;
	Byte reserved2[3];
	Qword serialNumber;
	Dword checksum;
	//}__attribute__ ((__packed__));
};

struct RecordHeaderNTFS
{
	char recordType[4];
	Word updateSequenceOffset;
	Word updateSequenceLength;
	Qword logFileSequenceNumber;
	Word recordSequenceNumber;
	Word hardLinkCount;
	Word attributesOffset;
	Word flags;
	Dword bytesInUse;
	Dword bytesAllocated;
	Qword parentRecordNumber;
	Dword nextRecordNumber;
	Dword reserved;
	Qword recordNumber;
	//}__attribute__ ((__packed__));
};

struct NTFSAttribute
{
	Dword type;
	Dword length;
	Byte formCode; // resident 0, non resident 1
	Byte nameLength;
	Word nameOffset;
	Word flags;
	Word attrID;
	Byte* data;


	void fill(Dword type, Dword length, Byte* data)
	{
		this->data = data;
		this->length = length;
		this->type = type;
	}

	void destroy()
	{
		delete[] data;
	}
};

struct NTFSAttrCustom
{
	NTFS_ATTR_TYPE type;
	Dword length;
	Byte formCode; // resident 0, non resident 1
	Byte nameLength;
	Word nameOffset;
	Word flags;
	Word attrID;
};

struct NTFSAttributeResident :public NTFSAttrCustom
{
	Dword contentLength;
	Word contentOffset;
	Byte padding;
	//Word unUsed;
	Byte* data;
};

struct NTFSAttributeNonResident : public NTFSAttrCustom
{
	Qword startVirtualCluster;
	Qword endingVirtualCluster;
	Word runlistOffset;
	Word compressionUnitSize;
	Dword reserved;
	Qword attrContentSize;
	Qword sizeOnDiskAttrContent;
	Qword initializedSizeOfAttrContent;
	Byte* data; //data runs  if first byte 00 END of runs
};

class NTFSRecord
{
	RecordHeaderNTFS header;
	std::vector<NTFSAttribute> attrs;

public:
	NTFSRecord(const RecordHeaderNTFS& header, const std::vector<NTFSAttribute>& attrs) :
		header(header),
		attrs(attrs)
	{}
	NTFSRecord()
	{}
};
#pragma pack(pop)