#include <iostream>
#include <cstdint>
#include <string.h>
#include <vector>

typedef int8_t  Byte;
typedef uint16_t Word;
typedef uint32_t Dword;
typedef uint64_t Qword;

//Own files
#include "NTFSStructures.hpp"
#include "FAT32Structures.hpp"
#include "NTFSLogic.hpp"
#include "Fat32Logic.hpp"
#include "Function.hpp"

#define FAT_PATH "dumps/fat.img"
#define NTFS_PATH "dumps/ntfs_full_512.img"

void readNTFS()
{
	NTFS ntfs(NTFS_PATH);
	ntfs.readRecords();
}

void readFAT()
{
	FILE* file =fopen(FAT_PATH, "rb");
	FatBPB bpb;
	FatEBP ebp;
}

int main(int argc, char **argv)
{
	readNTFS();
	std::cout << sizeof(BootSectorNTFS) << " " << sizeof(FatBPB) + sizeof(FatEBP);
	return 0;
}
