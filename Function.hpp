#pragma once
using namespace FAT;
//template<const int Size>
//Cluster<Size>* ReadCluster(FILE* file, int clusterNumber, int sectorSize, int clusterCount = 1, void* dest = nullptr)
//{
//	long ptr = ftell(file);
//	Cluster<Size>* cluster = nullptr;
//
//	fseek(file, clusterNumber*Size* sectorSize, SEEK_SET);
//
//	if (!dest)
//	{
//		cluster = new Cluster<Size>;
//		fread(cluster, Size*sectorSize, clusterCount, file);
//	}
//	else
//	{
//		fread(dest, clusterCount, 1, file);
//	}
//
//	//fseek(file, ptr, SEEK_SET);
//
//	return cluster;
//}
//
//template<const int Size>
//Sector<Size>* ReadSector(FILE* file, int sectorNumber, int sectorCounts)
//{
//	long ptr = ftell(file);
//	Sector<Size>* sector = new Sector<Size>;
//
//	if (sectorNumber)
//		sectorNumber--;
//
//	fseek(file, sectorNumber*Size, SEEK_SET);
//	fread(sector, sectorNumber*Size, 1, file);
//	fseek(file, ptr, SEEK_SET);
//
//	return sector;
//}

FatBPB *translateBPB(BootSectorNTFS* bs)
{
	FatBPB* bpb = new FatBPB;

	bpb->bytesPerSector = bs->bytesPerSector;
	bpb->sectorsPerCluster = bs->sectorPerCluster;
	bpb->reservedSectors = bs->reservedSectorCount; //;
	bpb->fatCopies = 2;
	bpb->rootDirEntries = bs->rootEntryCount;
	bpb->numSectors = bs->sectorCount;
	bpb->mediaType = bs->mediaType; //
	bpb->sectorsPerFat = bs->sectorPerTable;
	bpb->sectorsPerTrack = bs->sectorPerTrack;
	bpb->numberOfHeads = bs->heads;
	bpb->hiddenSectors = bs->hiddenSectorCount;
	bpb->sectorBig = 0;
	return bpb;
}

FatEBP *translateEBP(BootSectorNTFS* bs)
{
	FatEBP* ebp = new FatEBP;

	ebp->sectorPerFat = bs->sectorPerTable;// rzutowanie
	ebp->flags = 0; // not sure
	ebp->fatVersion = 0.0; //Not sure
	ebp->rootDirectoryClusterNumber = bs->rootEntryCount;//rzutowanie
	ebp->fsInfoSector = 0; // Not sure
	ebp->backupBootSectorNumber = 0; //not sure
	memset(ebp->Reserved, 0, sizeof(Byte) * 12);
	ebp->driveNumber = 0; // not sure
	ebp->flagsWindows = 0; // not sure
	ebp->signature = 0; // not sure
	ebp->volumeID = 0; // can be 0
	memset(ebp->volumeLabel, ' ', sizeof(Byte) * 11);
	ebp->systemString = (Qword) "FAT32";
	memset(ebp->bootCode, 0, sizeof(Byte) * 420); // not sure
	ebp->bootPartitionSignature = 0;// not sure
	return ebp;
}

