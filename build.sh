#!/bin/bash

CMAKE_DIRECTORY="CMakeSpace"
SOURCE_DIRECTORY="."

rm -rf bin
rm -rf CMAKE_DIRECTORY

eval "cmake -H$SOURCE_DIRECTORY -B$CMAKE_DIRECTORY"
eval "cmake --build $CMAKE_DIRECTORY -- -j3"
